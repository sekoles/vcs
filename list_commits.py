#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

import argparse
import csv
from importlib.resources import as_file
import logging
from datetime import datetime, timezone
from typing import Any

import gitlab
import urllib3
from gitlab import GitlabError, GitlabListError, GitlabAuthenticationError

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
urllib3.disable_warnings()


def write_csv(file_name: None, data: Any):
    try:
        logger.info(
            '[ * ] Запись полученных результатов в файл {}.csv ...'.format(file_name))
        with open('{}.csv'.format(file_name), mode='w', encoding='UTF8') as csv_file:
            writer = csv.writer(csv_file, dialect='excel')
            header = ("id", "repo", "sha", "short_sha", "datetime")
            writer.writerow(header)
            for d in data:
                writer.writerow(d)
        logger.info(
            '[ ✔ ] Запись полученных результатов в файл {}.csv ➜ ЗАВЕРШЕНА\n'.format(file_name))

    except Exception as exc:
        logger.error(exc)
        raise


def get_formated_now() -> str:
    str_now = datetime.now(timezone.utc).astimezone().isoformat()
    return str_now


def get_commits(project: Any) -> Any:
    result = []
    project_id = project.attributes.get('id')
    path_with_namespace = project.attributes.get(
        'path_with_namespace')
    logger.info(
        '[ * ] Получение списка коммитов в репозитории {} ...\n'.format(path_with_namespace))
    if project.commits:
        commits = []
        try:
            commits = project.commits.list(order_by='committed_date')
        except GitlabListError as exc:
            if exc.error_message.startswith('404 Repository Not Found'):
                err_res = (project_id, path_with_namespace,
                           exc.error_message, exc.response_code)
                result.append(err_res)
            else:
                logger.error(exc.response_code, exc.error_message)

        if commits:
            commit = commits[0]
            if commit:
                commit_id = commit.attributes.get('id')
                committed_date = commit.attributes.get('committed_date')
                commit_short_id = commit.attributes.get('short_id')
                commit_res = (project_id, path_with_namespace,
                              commit_id, commit_short_id, committed_date)
                result.append(commit_res)
        else:
            not_commits_res = (project_id, path_with_namespace,
                               'None', 'None', get_formated_now())
            result.append(not_commits_res)

    else:
        not_project_commits_res = (project_id, path_with_namespace,
                                   'None', 'None', get_formated_now())
        result.append(not_project_commits_res)

    logger.info('[ ✔ ] Получение списка коммитов в репозитории {} ➜ ЗАВЕРШЕНО\n'.format(
        path_with_namespace))
    return result


def create_report(gitlab_url: str, gitlab_token: str, dest_file_name: str):
    gl = gitlab.Gitlab(
        url=gitlab_url, private_token=gitlab_token, ssl_verify=False, user_agent='check_commits/0.0.1a')
    gl.auth()

    projects = []
    project_id = None
    logger.info('[ * ] Получение списка репозиториев в Gitlab ...\n')
    gl.projects.gitlab.auth()
    gl_projects = gl.projects.list(as_list=False)
    total_pages = gl_projects.total_pages
    n_page = 0

    for i in range(total_pages):
        n_page = i + 1
        try:
            if i < total_pages:
                items = gl.projects.list(as_list=False, page=n_page)
                for item in items:
                    projects.append(item)

        except GitlabAuthenticationError as exc:
            logger.error('project id {} : {}'.format(project_id, exc))

    if projects:
        projects_count = len(projects)
        logger.info(
            '[ ✔ ] Получено {} репозиториев в Gitlab  ➜ ЗАВЕРШЕНО\n'.format(projects_count))
        result = []
        for project in projects:
            if project:
                project_commits = None
                try:
                    project_commits = get_commits(project)
                    if project_commits:
                        for commit in project_commits:
                            result.append(commit)
                except GitlabError as exc:
                    logger.error(exc.response_code, exc.error_message)
                    raise

        if result:
            write_csv(dest_file_name, result)


def parse_args():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawTextHelpFormatter)
    auth = parser.add_mutually_exclusive_group(required=True)
    auth.add_argument('--token',
                      help='GitLab private token')
    parser.add_argument("--endpoint_url", metavar="endpoint_url",
                        default=None,
                        required=False,
                        help="GitLab endpoint url")
    parser.add_argument("--csv_file_name", metavar="csv_file_name",
                        default=None,
                        required=False,
                        help="Destination CSV file with repo commits")
    user_args = parser.parse_args()

    return user_args


def main():
    try:
        args = parse_args()
        gitlab_url = args.endpoint_url
        gitlab_token = args.token
        csv_file = args.csv_file_name

        create_report(gitlab_url=gitlab_url,
                      gitlab_token=gitlab_token, dest_file_name=csv_file)

    except Exception as exc:
        logger.error(exc)
        raise


if __name__ == '__main__':
    main()
